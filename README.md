# Template

This is a template for java projects:
* it has code coverage 100%
* it has a code formatter
* it has a gitlab CI setup

## Tech
- JUnit 4
- gradle 4.8
- Jacco
- Java 8
- Google formatter

